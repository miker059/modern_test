'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    less = require('gulp-less'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    plumber = require('gulp-plumber'),
    cssnano = require('gulp-cssnano'),
    svgSprite = require('gulp-svg-sprites'), //svg sprite
    svgmin = require('gulp-svgmin'),
    cheerio = require('gulp-cheerio'),
    replace = require('gulp-replace'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    htmlmin = require('gulp-htmlmin'),
    reload = browserSync.reload,
    path = {
        build: {
            html:   'build/',
            js:     'build/js/',
            css:    'build/css/',
            img:    'build/img/',
            sprite: 'build/img/sprite/'
        },
        src: { 
            html:   'src/*.html',
            js:     'src/js/scripts.js',
            less:   'src/less/styles.less',
            img:    'src/img/**/*.*',
            sprite: 'src/sprite/*.svg'
        },
        watch: {
            html:   'src/**/*.html',
            js:     'src/js/**/*.js',
            less:   'src/less/**/*.less',
            libs:   'bower_components/bootstrap-less/less/**/*.less',
            img:    'src/img/**/*.*',
            sprite: 'src/sprite/*.svg'
        },
        clean: './build'
    },
    config = {
        server: {
            baseDir: "./build"
        },
        //tunnel: true,
        host: 'localhost',
        port: 3000,
        logPrefix: "miker059"
    };

// build partials
gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        //.pipe(htmlmin({collapseWhitespace: true}))  //minify index.html
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
    gulp.src(path.src.less)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(prefixer())
        .pipe(cssnano())
        .pipe(sourcemaps.write('maps'))
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.css)) 
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

// SVG Sprites
gulp.task('sprite:build', function () {
    gulp.src(path.src.sprite)
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[style]').removeAttr('style');
            },
            parserOptions: { xmlMode: true }
        }))
        .pipe(replace('&gt;', '>'))
        .pipe(svgSprite({
                mode: "symbols",
                preview: false,
                selector: "icon-%f",
                svg: {
                    symbols: 'sprite.html'
                }
            }
        ))
        .pipe(gulp.dest(path.build.sprite))
        .pipe(reload({stream: true}));
});

// Run tasks
gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'image:build',
    'sprite:build' //svg sprite
]);

// watch
gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.less, path.watch.libs], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.sprite], function(event, cb) { //svg sprite
        gulp.start('sprite:build');                  //svg sprite
    });                                              //svg sprite
});

// server livereload
gulp.task('server', function () {
    browserSync(config);
});

// clean
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

// default
gulp.task('default', ['build', 'server', 'watch']);